import { Router } from 'express';
import passport from 'passport';
import multer from '../config/multer';
import { createProduct, getProducts } from '../controllers/product.controller';

const router = Router();

router.post('/products', multer.single('image'),createProduct);
router.get('/products',getProducts);
// router.get('/products', passport.authenticate('jwt',{session: false}));

export default router;