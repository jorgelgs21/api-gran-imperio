import { Router } from 'express';
import { 
    singUp, 
    singIn,
    sendVerify,
    verify,
    forgotPassword,
    changePassword,
    validUser
 } from '../controllers/user.controller';
 
const router = Router();

router.post('/signup', singUp);
router.post('/signin', singIn);
router.post('/send-verify', sendVerify);
router.post('/verify', verify);
router.post('/forgot-password', forgotPassword);
router.post('/new-password', changePassword);
router.post('/valid-user', validUser);

export default router;