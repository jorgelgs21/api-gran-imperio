import {Schema, model ,Document} from 'mongoose';

export interface IProduct extends Document{
    name    :string
    price   :number
    img     :string
}

const productShema = new Schema({
    name: {
        type: String
    },
    price: {
        type: Number
    },
    img: {
        type: String
    }
});

export default model<IProduct>('Product',productShema);
