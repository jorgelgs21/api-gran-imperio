import {model, Schema, Document} from 'mongoose';
import bcrypt from 'bcrypt';

export interface IUser extends Document {
    first_name  : string,
    last_name   : string,
    email       : string,
    password    : string,
    active      : boolean,
    comparePassword: (password:string) => Promise<boolean>,
    updatePassword: (password:string) => Promise<boolean>,
}

const userSchema = new Schema({
    first_name: {
        type        : String,
        required    : true,
        lowercase   : true,
        trim        : true,
        minlength   : 3,
        maxlength   : 15
    },
    last_name: {
        type        : String,
        required    : true,
        lowercase   : true,
        trim        : true,
        minlength   : 3,
        maxlength   : 15
    },
    email: {
        type        : String,
        unique      : true,
        required    : true,
        lowercase   : true,
        trim        : true,
        maxlength   : 30
    },
    password:{
        type        : String,
        required    : true,
        minlength   : 8,
        maxlength   : 30
    },
    active:{
        type        : Boolean,
        default     : false
    },
    creation_date:{
        type        : Date,
        default     : new Date()
    }
});

userSchema.pre<IUser>('save', async function (next) {
    if (!this.isModified('password')) return next();

    const salt = await bcrypt.genSalt(10);
    const hash = await bcrypt.hash(this.password, salt)
    this.password = hash;
});

userSchema.methods.comparePassword = async function (password:string):Promise<boolean> {
    console.log(this.password);
    console.log(password);
    
    return await bcrypt.compare(password, this.password);
};

userSchema.methods.updatePassword = async function (password:string):Promise<boolean> {
    const salt = await bcrypt.genSalt(10);
    const hash = await bcrypt.hash(password, salt);
    return await this.update({$set:{password:hash}});
}

export default model<IUser>('User', userSchema);