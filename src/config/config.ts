export default {
    jwtSecret: process.env.JWT_SECRET || 'anais#01',
    DB:{
        URI: process.env.MONGODB_URI || 'mongodb://localhost/apiGranImperio',
        USER: process.env.MONGODB_USER,
        PASSWORD: process.env.MONGODB_PASSWORD
    }
}