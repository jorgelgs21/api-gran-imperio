import nodemailer from 'nodemailer';

export async function configTransport(){
    let testAccount = await nodemailer.createTestAccount();
    return nodemailer.createTransport({
        host: "smtp.ethereal.email",
        port: 587,
        secure: false, // true for 465, false for other ports
        auth: {
          user: testAccount.user, // generated ethereal user
          pass: testAccount.pass, // generated ethereal password
        },
    })
}

export function createBodyMail(
    to      :string, 
    subject :string, 
    textHtml:string, 
    from    :any = null){

    return {
        from    : from || 'admin@cocuygranimperio.com',
        to      : to,
        subject : subject,
        html    : textHtml
    }
}

export function getTestMessageUrl(info:any) {
    return nodemailer.getTestMessageUrl(info);
}