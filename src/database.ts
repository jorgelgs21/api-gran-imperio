import mongoose, { ConnectionOptions } from 'mongoose';
import config from './config/config';

const dbOption: ConnectionOptions = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true
}

mongoose.connect(config.DB.URI, dbOption);

mongoose.connection.once('open',() =>{
    console.log('Mongodb connect');
});

mongoose.connection.on('error',(err)=>{
    console.log(err);
    process.exit(0);
})