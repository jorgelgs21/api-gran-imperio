import {Request, Response} from 'express';
import User, {IUser} from '../models/user';
import jwt from 'jsonwebtoken';
import config from '../config/config';

function createToken(user:IUser){
    return jwt.sign({
        _id: user._id,
        first_name: user.first_name,
        last_name: user.last_name,
        email: user.email
    },config.jwtSecret,{
        expiresIn: 86400
    })
}

function emptyUser(body:IUser){
    if (!body.first_name || !body.last_name || !body.email || !body.password) {
        return true
    }
    return false
}

function validateUser(body:IUser):any{
    let user = {
        first_name: body.first_name.trim().toLowerCase(),
        last_name: body.last_name.trim().toLowerCase(),
        email: body.email.trim().toLowerCase(),
        password: body.password.trim().toLowerCase(),
    }
    let errors:Array<string> = [];
    if (user.first_name.length < 3 || user.first_name.length > 15) {
        errors.push('length name is invalid')
    }
    if (user.last_name.length < 3 || user.last_name.length > 15) {
        errors.push('length surname is invalid')
    }
    if (user.email.length > 30) {
        errors.push('length email is invalid')
    }
    if (user.password.length < 8 || user.password.length > 30) {
        errors.push('length password is invalid')
    }
    
    if (errors.length > 0) {
        return {
            valid: false,
            errors: errors
        }
    }else{
        return {
            valid: true,
            user:  user
        }
    }
}


function responseMail(info:any, res:Response){
    if (info) {
        return res.status(200).json({
            status  : 200, 
            msg     :'mensaje enviado',
            mail    : info.messageId,
            url     : getTestMessageUrl(info)
        });
    }
    return res.status(400).json({status:400, mail: 'invalid', msg:'Error al enviar correo'});
}

export const singUp = async (req: Request, res:Response):Promise<Response> => {
    if (emptyUser(req.body)) {
        return res.status(400).json({status: 400, msg: 'se requiere nombre, apellido, correo y clave'})
    }

    let valid_body:any = validateUser(req.body);

    if (valid_body.valid) {
        let exists = await User.findOne({email: valid_body.user.email})
        if (exists) {
            return res.status(400).json({
                status: 400, 
                msg: 'el correo ya se encuentra registrado',
                exists: true
            })
        }
    
        let user:IUser = new User(valid_body.user);
        await user.save();
        return res.status(201).json({
            status: 201, 
            msg:'creado con exito',
            data: {
                _id         : user._id,
                first_name  : user.first_name,
                last_name   : user.last_name,
                email       : user.email
            }
        });
    }else{
        return res.status(400).json({status: 400, msg: valid_body.errors});
    }
}


export const singIn = async (req: Request, res:Response) => {
    if (!req.body.email || !req.body.password) {
        return res.status(400).json({status: 400, msg: 'Se requiere usuario y clave'})
    }

    let user = await User.findOne({email: req.body.email})
    if (!user) {
        return res.status(400).json({status: 400, msg: 'Correo o clave invalida'})
    }

    let isMatch = await user.comparePassword(req.body.password);
    if (!isMatch) {
        return res.status(400).json({status: 400, msg: 'Correo o clave invalida'})
    }

    let result = await User.findById(user._id,{password:0})
    return res.status(200).json({
        status:200,
        msg: `Bienvenido ${user.first_name} ${user.last_name}`,
        data: {
            user    : result,
            token   : createToken(user)
        }
    });
}

import { configTransport, createBodyMail , getTestMessageUrl} from '../config/nodemailer';


export const sendVerify = async (req: Request, res:Response) => {
    
    let transporter = await configTransport();
    
    let link = `${req.get('Origin')}/signin/${createToken(req.body)}`;
    let subject = 'Por favor confirme su cuenta de correo';
    let textHtml = `<p><b><i>Hola ${req.body.first_name},</i></b></p>
                    <p>Por favor has click en el siguiente link para verificar tu cuenta.</p>
                    <a href="${link}">Click verificación de cuenta!</a>`;
    let body = createBodyMail(req.body.email, subject, textHtml);

    let info = await transporter.sendMail(body);
    responseMail(info,res);
}

export const verify = async (req: Request, res:Response) => {
    let user = await verifyToken(req.body.token)
    if (user) {
        await user.update({$set:{active:true}});
        return res.status(201).json({
            status  : 201,
            msg     :'cuenta verificada exitosamente',
            data    : {
                _id         : user._id,
                first_name  : user.first_name,
                last_name   : user.last_name,
                email       : user.email,
            }
        });
    }
    return res.status(400).json({
        status  : 400,
        msg     : 'Token invalido'
    })
}

export const forgotPassword = async (req: Request, res:Response):Promise<Response> => {
    
    if (req.body.email) {
        let user = await User.findOne({email: req.body.email});
        console.log(user);
        
        if (user) {
            let transporter = await configTransport();
            let link = `${req.get('Origin')}/new-password/${createToken(user)}`;
            let subject = 'Recuperacion de Contraceña';
            let textHtml = `<p><b><i>Hola ${user.first_name},</i></b></p>
                            <p>Por favor has click en el siguiente link para recuperar tu contraceña.</p>
                            <a href="${link}">Click verificación de cuenta!</a>`;
            let body = createBodyMail(user.email, subject, textHtml);

            let info = await transporter.sendMail(body);
            return responseMail(info,res);
        }
        return res.status(400).json({status:400, msg:'Este correo no esta registrado'});
    }
    return res.status(400).json({status:400, msg:'Se requiere el correo del usuario'});
}

export const changePassword = async (req:Request, res:Response)=>{
    let user = await verifyToken(req.body.token);
    if (user) {
        let update:any = await user.updatePassword(req.body.password);
        if (update.nModified) {
            return res.status(201).json({status:201, msg:'cambio de contraceña exitoso'})
        }
        return res.status(400).json({status:400, msg:'hubo un error al cambiar la contraceña'})
    }
    return res.status(400).json({
        status  : 400,
        msg     : 'Token invalido'
    })
}

export const validUser = async (req:Request, res:Response)=>{
    let user = await verifyToken(req.body.token);
    if (user) {
        return res.status(200).json({
            status:200,
            msg: 'Usuario valido'
        })
    }
    return res.status(400).json({
        status  : 400,
        msg     : 'Token invalido'
    })
}

async function verifyToken(token:string){
        let data:any = null;  
        try {
            data = <any>jwt.verify(token, config.jwtSecret); 
            if (data) {
                let user:IUser|null = await User.findById(data._id);
                if (user) return user
            }            
            return null;
        } catch (error) {
            return null;            
        }
}