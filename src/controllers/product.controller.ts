import { Request, Response } from 'express';
import Product from '../models/product';

export const createProduct = async (req:Request, res:Response):Promise<Response>=>{
    console.log(req.file)
    if (req.file) {
        if (req.body.name && req.body.price) {
            let newProduct = new Product({
                name: req.body.name,
                price: req.body.price,
                img: req.file.path
            });

            await newProduct.save();
            return res.status(201).json({
                status: 201,
                msg: 'producto creado',
                data: newProduct
            });
        }
        return res.status(400).json({
            status: 400,
            msg: 'se requiere nombre, precio y descripcion del producto'
        });
    }
    return res.status(400).json({
        status: 400,
        msg: 'se requiere imagen del producto'
    });
}

export const getProducts = async (req:Request, res:Response):Promise<Response> => {
    let products = await Product.find();
    if (products) {
        return res.status(200).json({
            status: 200,
            data: products
        })
    }
    return res.status(400).json({
        status: 400,
        msg: "no existen productos"
    })
}